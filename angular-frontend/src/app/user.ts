export class User {
  id: number;
  nickName: string;
  email: string;
  password: string;


  constructor(id: number, nickName: string, email: string, password: string) {
    this.id = id;
    this.nickName = nickName;
    this.email = email;
    this.password = password;
  }

// userTypeId: number;
}
